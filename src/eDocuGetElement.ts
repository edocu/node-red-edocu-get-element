import { NodeProperties, Red } from "node-red";
import { Node } from "node-red-contrib-typescript-node";
import axios from "axios";

interface getElementProps {
  server: {
    url: string,
    getElementUrl: string,
    cookie: string
  },
  uid: string
  hash: string
  elementType: string
  accept: string
  query?: string
  jwt: string
}

type props = getElementProps & NodeProperties

type getElementOutput = {
  element?: any
  error?: any
  statusCode: number
  headers: any
}

export = function(RED: Red) {
  class LowerCaseNode extends Node {
    props: getElementProps;
    constructor(config: props) {
      super(RED);

      this.createNode(config);
      this.props = {...config};
      this.props.server = RED.nodes.getNode(config.server as any) as any;

      this.on("input", async (msg: any) => {
        try {
          // this.status({fill: "red", shape: "ring", text: JSON.stringify(this.props.server.cookie)});
          // this.send({payload: JSON.stringify(this.props.server.cookie)});
          const res = await this.getElement()

          const output: getElementOutput = {statusCode: res.status, headers: res.headers}
          if (res.status === 200) {
            output.element = res.data;
          }
          else {
            output.error = res.data;
          }
          this.send({payload: JSON.stringify(output)});
          this.status({fill: "green", shape: "ring", text: "sent" });
        } catch (error) {
          this.status({fill: "red", shape: "ring", text: String(error)});
        }
      });
    }

    getElement() {
      const instance = axios.create({
        baseURL: `${this.props.server.url}/`,
        headers: {
          //UID: this.props.uid,
          Cookie: this.props.server.cookie,
          Accept: this.props.accept === undefined ? undefined : this.props.accept
        }
      });

      return instance.get(`${this.props.server.getElementUrl}/${this.props.elementType}/${this.props.hash}`, {
        params: this.props.query === undefined ? undefined : this.props.query
      });
    }
  }

  LowerCaseNode.registerType(RED, "eDocuGetElement");
};
